package com.yunze.common.exception;

/**
 * 演示模式异常
 * 
 * @author yunze
 */
public class DemoModeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public DemoModeException()
    {
    }
}
