package com.yunze.common.enums;

/**
 * 数据源
 * 
 * @author yunze
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
